from django.urls import path
from .views import show_todo_lists, show_todo_list_detail, create_todo_list, change_todo, delete_todo_list, create_todo_item

urlpatterns = [
    path('', show_todo_lists, name='todo_list_list'),
    path('<int:id>/edit/', change_todo, name='todo_list_update'),
    path('<int:id>/delete/', delete_todo_list, name='todo_list_delete'),
    path('create/', create_todo_list, name='todo_list_create'),
    path('<int:id>/', show_todo_list_detail, name='todo_list_detail'),path('items/create/', create_todo_item, name='todo_item_create'),
]
