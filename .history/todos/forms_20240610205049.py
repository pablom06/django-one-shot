from django import forms
from .models import TodoList

class TodoListForm(forms.ModelForm):
    list = forms.ModelChoiceField(queryset=TodoList.objects.all())
    class Meta:
        model = TodoList
        fields = ['name', 'list'
