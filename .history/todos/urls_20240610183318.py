from django.urls import path
from .views import show_todo_lists, show_todo_list_detail, create_todo_list

urlpatterns = [
    path('', show_todo_lists, name='todo_list_list'),
    path('create/', change_todo, name='change_todo
    path('create/', create_todo_list, name='todo_list_create'),
    path('<int:id>/', show_todo_list_detail, name='todo_list_detail'),
]
