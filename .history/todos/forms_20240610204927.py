from django import forms
from .models import TodoList

class TodoListForm(forms.ModelForm):
    list = 
    class Meta:
        model = TodoList
        fields = ['name']
