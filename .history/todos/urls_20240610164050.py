from django.urls import path
from .views import show_todo_lists, show_todo_list_detail

urlpatterns = [
    path('', show_todo_lists, name='todo_list_list'),
     path('create/', create_todo_list, name='todo_list_create'),
    path('<int:id>/', show_todo_list_detail, name='todo_list_detail'),
]
