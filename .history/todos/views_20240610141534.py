from django.shortcuts import render
from django.contrib.auth.models import User
from .m

def show_todo_lists(request):
    user = User.objects.first()
    return render(request, 'todos/todo_lists.html', {'user': user})
