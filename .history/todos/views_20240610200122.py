from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.models import User
from .models import TodoList, TodoItem
from django.urls import reverse
from .forms import TodoListForm

def show_todo_lists(request):
    todo_lists = TodoList.objects.all()
    return render(request, 'todos/todo_lists.html', {'todo_lists': todo_lists})

def show_todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    todo_items = todo_list.items.all()
    return render(request, 'todos/todo_list_detail.html', {'todo_list': todo_list, 'todo_items': todo_items})


def create_todo_list(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect('todo_list_detail', id=todo_list.id)
    else:
        form = TodoListForm()
    return render(request, 'todos/todo_list_create.html', {'form': form})

def change_todo(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)

    return render(request, 'todos/change_todo.html', {'form': form})

def delete_todo_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
if request.method == 'POST':
    todo_list.delete()
    return redirect('todo_list_delete')
