from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User
from .models import TodoList, TodoItem

def show_todo_lists(request):
    todo_lists = TodoList.objects.all()
    return render(request, 'todos/todo_lists.html', {'todo_lists': todo_lists})

def show_todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    todo_items = todo_list.items.all()
    return render(request, 'todos/todo_list_detail.html', {'todo_list': todo_list, 'todo_items': todo_items})

def create_todo_list(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        user = User.objects.first()
        TodoList.objects.create(name=name, user=user)
        return redirect('todo_list_list')
    return render(request, 'todos/create_todo_list.html')
