from django.shortcuts import render
from django.contrib.auth.models import User
from .models import TodoList, TodoItem

def show_todo_lists(request):
    user = User.objects.first()
    return render(request, 'todos/todo_lists.html', {'user': user})
