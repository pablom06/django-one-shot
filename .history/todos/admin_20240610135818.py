from django.contrib import admin
from .models import TodoList

class TodoListAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')

admin.site.register(TodoList, TodoListAdmin)

class TodoItemAdmin(admin.ModelAdmin):
    list_display = ('task', 'due_date',)
