from django.shortcuts import render
from django.contrib.auth.models import User
from .models import TodoList, TodoItem

def show_todo_lists(request):
    todo_lists = TodoList.objects.all()
    return render(request, 'todos/todo_lists.html', {'todo_lists': todo_lists})

def show_todo_list(request, list_id):
    todo_list = TodoList.objects.get(pk=list_id)
    return render(request, 'todos/todo_list.html', {'todo_list': todo_list})
