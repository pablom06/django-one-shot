from django.contrib import admin
from .models import TodoList

class TodoListAdmin(admin.ModelAdmin):
    list_display = ('created_on',)
