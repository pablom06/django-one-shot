from django.urls import path
from .views import show_todo_lists, show_todo_list_detail, create_todo_list, change_todo

urlpatterns = [
    path('', show_todo_lists, name='todo_list_list'),
    path('<int:id>/edit', change_todo, name='todo'),
    path('create/', create_todo_list, name='todo_list_create'),
    path('<int:id>/', show_todo_list_detail, name='todo_list_detail'),
]
