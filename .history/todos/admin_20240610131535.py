from django.contrib import admin
from .models import TodoList

class TodoListAdmin(admin.ModelAdmin):
    readonly_fields = ('created_on',)
