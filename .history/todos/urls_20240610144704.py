from django.urls import path
from .views import show_todo_lists

urlpatterns = [
    path('', show_todo_lists, name='todo_list_list'),
]
