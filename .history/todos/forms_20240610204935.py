from django import forms
from .models import TodoList

class TodoListForm(forms.ModelForm):
    list = forms.ModelChoiceField(queryset=TodoList.objects.all(), empty_label='Select a list', required=False)
    class Meta:
        model = TodoList
        fields = ['name']
